<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'description' => $faker->paragraphs(rand(2,10), true),
        'date' => $faker->dateTime(),
        'image' => $faker->imageUrl(600, 400, 'cats'),
    ];
});
