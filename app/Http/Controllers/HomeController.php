<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class HomeController extends Controller
{
    
    
    public function blog()
    {
     	$blog = Blog::orderBy('date', 'desc')->paginate(5);

     	return view('home', ['blog' => $blog]);
    }

    public function article($id){

    
		$article = Blog::findOrFail($id);

     	return view('article', ['article' => $article]);
    }

    public function subdomains()
    {
       echo 'subdomains';
    }
}
