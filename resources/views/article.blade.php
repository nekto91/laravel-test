@extends('layouts.app')

@section('content')


  
            
        <div class="container">
			 
			    	<div class="row">
			    		<div class="col-md-4">
			    			<img src="{{ $article->image }}" class="img-fluid" alt="{{ $article->title }}">
			    		</div>
			    		<div class="col-md-8">
			    			<h3>{{ $article->title }}</h3>
				    	
				    			<p><small>{{ $article->date }}</small></p>
				    		
				    		<p>{{ $article->description }}</p>
			    		</div>
			    	</div>
			        
			   
			</div>


@endsection
