@extends('layouts.app')

@section('content')


  
            
        <div class="container">
			    @foreach ($blog as $item)
			    	<div class="mb-5">
			    		<div class="row">
				    		<div class="col-md-4">
				    			<img src="{{ $item->image }}" class="img-fluid" alt="{{ $item->title }}">
				    		</div>
				    		<div class="col-md-8">
				    			<h3><a href="/blog/{{ $item->id }}">{{ $item->title }}</a></h3>
					    	
					    			<p><small>{{ $item->date }}</small></p>
					    		
					    		<p>{{ Str::limit($item->description, 300) }}</p>
				    		</div>
				    	</div>
			    	</div>
			        
			    @endforeach

			    {{ $blog->links() }}
			</div>


@endsection
